package com.example.administrator.demo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class scene4 extends AppCompatActivity implements View.OnClickListener{
    ImageButton play, pause, stop;
    Button introduction;
    MediaPlayer mediaPlayer;
    int pauseCurrentPosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scene4);

        TextView textView = findViewById(R.id.intro4);
        String text = "Listen out for the melancholic wail of the Bush Stone-curlew, Sugar Gliders yapping to each other in search of sap, possums fighting in the trees and the high-pitched ‘ting’ of microbats mid-air. A Boobook Owl swoops down to catch its prey, an Owlet Nightjar sharply purrs in search of insects. The hypnotic croak of frogs in search of romance vibrate lowly through the air. Cradling the eucalypt-drenched air is the rhythmic chirp of nocturnal insects.\n" +
                "\n" +
                "Although there are not as many species here as there were 200 years ago, we have more species and more animals than most Grassy Woodland environments. Our conservation efforts have seen the return of the Eastern Bettong, New Holland Mouse, Bush Stone-curlew and Eastern Quoll. The animals that have persisted in the Sanctuary are flourishing without the threat of foxes, cats and rabbits.\n" +
                "\n" +
                "Listen for the different animals in this soundscape\n" +
                "\n" +
                "1.\tBush Stone-curlew (reintroduced in 2014)\n" +
                "2.\tSugar Glider\n" +
                "3.\tSouthern Boobook Owl\n" +
                "4.\tTawny Frogmouth\n" +
                "5.\tBrushtail Possum\n" +
                "6.\tMicrobat\n" +
                "7.\tAustralian Owlet-Nightjar\n" +
                "8.\tSpotted Marsh Frog\n" +
                "9.\tCommon Eastern Froglet\n" +
                "10.\tEmerald Spotted Tree Frog\n";

        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene4.this, Animal1.class);
                startActivity(intent);
            }
        };
        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene4.this, Animal6.class);
                startActivity(intent);
            }
        };

        ss.setSpan(clickableSpan1, 897, 919, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 1062, 1083, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());


        play = (ImageButton) findViewById(R.id.button2);
        pause = (ImageButton) findViewById(R.id.pausebtn);
        stop = (ImageButton) findViewById(R.id.stopbtn);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        stop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.button2:
                if(mediaPlayer==null) {
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.scene4);
                    mediaPlayer.start();
                }
                else if(!mediaPlayer.isPlaying()){
                    mediaPlayer.seekTo(pauseCurrentPosition);
                    mediaPlayer.start();
                }
                break;

            case R.id.pausebtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.pause();
                    pauseCurrentPosition=mediaPlayer.getCurrentPosition();

                }

                break;

            case R.id.stopbtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.stop();
                    mediaPlayer = null;
                }
                break;

        }
    }
}