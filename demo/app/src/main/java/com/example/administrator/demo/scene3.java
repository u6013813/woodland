package com.example.administrator.demo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class scene3 extends AppCompatActivity implements View.OnClickListener{
    ImageButton play, pause, stop;
    Button introduction;
    MediaPlayer mediaPlayer;
    int pauseCurrentPosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scene3);

        TextView textView = findViewById(R.id.intro3);
        String text = "Sadly, today’s dawn chorus is missing some of the species that would have been here 100 years ago. In addition, we now have some introduced species that have come from overseas.\n" +
                "\n" +
                "With loss of habitat, foxes, cats and new birds on the block, the diversity and abundance of birds has declined. However, with the great restoration work done by ecologists, community, volunteers and Government, we are able to protect what currently lives at Mulligans Flat and reintroduce species that have disappeared.\n" +
                "\n" +
                "Listen out for all the different animals in this soundscape\n" +
                "\n" +
                "1.\tYellow-rumped Thornbill\n" +
                "2.\tScarlet Robin\n" +
                "3.\tGrey Butcherbird\n" +
                "4.\tCommon Starling (introduced species)\n" +
                "5.\tCommon Myna (introduced species)\n" +
                "6.\tNoisy Miner (introduced species)\n" +
                "7.\tKookaburra\n" +
                "8.\tWhite-winged Chough\n" +
                "9.\tCrimson Rosella\n" +
                "10.\tWhite-throated Gerygone\n" +
                "11.\tTree Cricket\n" +
                "12.\tSuperb Parrot\n" +
                "13.\tSpotted Pardalote\n" +
                "14.\tNoisy Friarbird\n" +
                "15.\tMagpie\n" +
                "16.\tWillie Wagtail\n" +
                "17.\tCrimson Rosella\n" +
                "18.\tGrey Fantail\n" +
                "19.\tRufous Whistler\n" +
                "20.\tHorsfield’s Bronze-cuckoo\n" +
                "21.\tFan-tailed Cuckoo\n" +
                "22.\tAustralian Raven\n" +
                "23.\tGrey Currawong\n" +
                "24.\tRed Wattlebird\n" +
                "25.\tGrey Shrike-thrush\n" +
                "26.\tBuff-rumped Thornbill\n" +
                "27.\tWhite-throated Treecreeper\n" +
                "28.\tBrown-headed Honeyeater\n" +
                "29.\tCommon Bronzewing Pigeon\n";

        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene3.this, Animal5.class);
                startActivity(intent);
            }
        };
        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene3.this, Animal2.class);
                startActivity(intent);
            }
        };
        ClickableSpan clickableSpan3 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene3.this, Animal4.class);
                startActivity(intent);
            }
        };

        ss.setSpan(clickableSpan1, 589, 605, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 839, 856, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan3, 965, 985, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());


        play = (ImageButton) findViewById(R.id.button2);
        pause = (ImageButton) findViewById(R.id.pausebtn);
        stop = (ImageButton) findViewById(R.id.stopbtn);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        stop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.button2:
                if(mediaPlayer==null) {
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.scene3);
                    mediaPlayer.start();
                }
                else if(!mediaPlayer.isPlaying()){
                    mediaPlayer.seekTo(pauseCurrentPosition);
                    mediaPlayer.start();
                }
                break;

            case R.id.pausebtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.pause();
                    pauseCurrentPosition=mediaPlayer.getCurrentPosition();

                }

                break;

            case R.id.stopbtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.stop();
                    mediaPlayer = null;
                }
                break;

        }
    }
}