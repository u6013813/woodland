package com.example.administrator.demo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class scene2 extends AppCompatActivity implements View.OnClickListener{
    ImageButton play, pause, stop;
    Button introduction;
    MediaPlayer mediaPlayer;
    int pauseCurrentPosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scene2);

        TextView textView = findViewById(R.id.intro2);
        String text = "200 years ago the nighttime rustled, cooed, croaked and chirped much louder – with more animals and different species. The reason we know this is because of studies on owl pellets – these are fossilised bone deposits leftover from the meals of owls. Pellets have revealed over 15 different small mammals that would have called Mulligans Flat home. Some of these furry critters include the Feathertail Glider, Eastern Bettong, Eastern Quoll, Southern Brown Bandicoot, Agile Antechinus and the Eastern Pygmy Possum.\n" +
                "\n" +
                "Sadly – largely due to the reintroduction of foxes and cats – we have lost these species. However, with our predator-proof fence, and innovative conservation work, we are starting to bring them back. The Eastern Bettong was reintroduced in 2012, New Holland Mouse in 2013, Bush Stone-curlew in 2014 and Eastern Quoll in 2016. We will continue to reintroduce missing species to the Sanctuary, so be prepared to hear a different soundscape as the years pass.\n" +
                "\n" +
                "Listen out for the different animals in this soundscape\n" +
                "\n" +
                "\n" +
                "*many of the species that would have been here do not make an audible sound and could not be included in this soundscape\n" +
                "\n" +
                "\n" +
                "1.\tGreen and Golden Bell Frog (now locally extinct)\n" +
                "2.\tBarking Owl (now locally extinct)\n" +
                "3.\tBush Stone-curlew (locally extinct 50 years ago, reintroduced in 2014)\n" +
                "4.\tSquirrel Glider (now locally extinct)\n" +
                "5.\tSugar Glider\n" +
                "6.\tSouthern Boobook Owl\n" +
                "7.\tTawny Frogmouth\n" +
                "8.\tMicrobat\n" +
                "9.\tAustralian Owlet-Nightjar\n" +
                "10.\tSpotted Marsh Frog\n" +
                "11.\tCommon Eastern Froglet\n" +
                "12.\tEmerald Spotted Tree Frog\n" +
                "13.\tBrushtail Possum\n";

        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene2.this, Animal1.class);
                startActivity(intent);
            }
        };
        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene2.this, Animal6.class);
                startActivity(intent);
            }
        };

        ss.setSpan(clickableSpan1, 1243, 1263, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 1458, 1481, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());


        play = (ImageButton) findViewById(R.id.button2);
        pause = (ImageButton) findViewById(R.id.pausebtn);
        stop = (ImageButton) findViewById(R.id.stopbtn);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        stop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.button2:
                if(mediaPlayer==null) {
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.scene2);
                    mediaPlayer.start();
                }
                else if(!mediaPlayer.isPlaying()){
                    mediaPlayer.seekTo(pauseCurrentPosition);
                    mediaPlayer.start();
                }
                break;

            case R.id.pausebtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.pause();
                    pauseCurrentPosition=mediaPlayer.getCurrentPosition();

                }

                break;

            case R.id.stopbtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.stop();
                    mediaPlayer = null;
                }
                break;

        }
    }
}