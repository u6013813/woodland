package com.example.administrator.demo;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import static android.Manifest.permission.CAMERA;

public class ScanQRCode extends AppCompatActivity implements ZXingScannerView.ResultHandler{

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(checkPermission())
            {
                Toast.makeText(ScanQRCode.this, "Permission is granted", Toast.LENGTH_LONG).show();
            }
            else
            {
                requestPermission();
            }
        }
    }

    private boolean checkPermission()
    {
        return (ContextCompat.checkSelfPermission(ScanQRCode.this, CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission()
    {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    public void onRequestPermissionResult(int requestCode, String permission[], int grantResults[])
    {
        switch(requestCode)
        {
            case REQUEST_CAMERA:
                if(grantResults.length > 0)
                {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if(cameraAccepted)
                    {
                        Toast.makeText(ScanQRCode.this, "Permission Granted", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(ScanQRCode.this, "Permission Denied", Toast.LENGTH_LONG).show();
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        {
                            if(shouldShowRequestPermissionRationale(CAMERA))
                            {
                                displayAlertMessage("You need to allow access for both permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if(checkPermission())
            {
                if(scannerView == null)
                {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();
            }
            else
            {
                requestPermission();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    public void displayAlertMessage(String message, DialogInterface.OnClickListener listener)
    {
        new AlertDialog.Builder(ScanQRCode.this)
                .setMessage(message)
                .setPositiveButton("OK",listener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void handleResult(final Result result) {
        final String scanResult = result.getText();
        //AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (scanResult.equals("A1")) {
            Intent intent = new Intent(ScanQRCode.this,Animal1.class);
            startActivity(intent);
        }
        if (scanResult.equals("A2")) {
            Intent intent = new Intent(ScanQRCode.this,Animal2.class);
            startActivity(intent);
        }
        if (scanResult.equals("A3")) {
            Intent intent = new Intent(ScanQRCode.this,Animal3.class);
            startActivity(intent);
        }
        if (scanResult.equals("A4")) {
            Intent intent = new Intent(ScanQRCode.this,Animal4.class);
            startActivity(intent);
        }
        if (scanResult.equals("A5")) {
            Intent intent = new Intent(ScanQRCode.this,Animal5.class);
            startActivity(intent);
        }
        if (scanResult.equals("A6")) {
            Intent intent = new Intent(ScanQRCode.this,Animal6.class);
            startActivity(intent);
        }
        if (scanResult.equals("S1")) {
            Intent intent = new Intent(ScanQRCode.this,scene1.class);
            startActivity(intent);
        }
        if (scanResult.equals("S2")) {
            Intent intent = new Intent(ScanQRCode.this,scene2.class);
            startActivity(intent);
        }
        if (scanResult.equals("S3")) {
            Intent intent = new Intent(ScanQRCode.this,scene3.class);
            startActivity(intent);
        }
        if (scanResult.equals("S4")) {
            Intent intent = new Intent(ScanQRCode.this,scene4.class);
            startActivity(intent);
        }
        else scannerView.resumeCameraPreview(ScanQRCode.this);

        //AlertDialog alert = builder.create();
        //alert.show();
    }
}
