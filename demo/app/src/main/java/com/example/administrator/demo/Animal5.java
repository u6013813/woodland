package com.example.administrator.demo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Animal5 extends AppCompatActivity implements View.OnClickListener{
    ImageButton play, pause, stop;
    Button introduction;
    MediaPlayer mediaPlayer;
    int pauseCurrentPosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal5);

        introduction = (Button) findViewById(R.id.introbtn);
        play = (ImageButton) findViewById(R.id.button2);
        pause = (ImageButton) findViewById(R.id.pausebtn);
        stop = (ImageButton) findViewById(R.id.stopbtn);

        introduction.setOnClickListener(this);
        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        stop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.button2:
                if(mediaPlayer==null) {
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.animal5);
                    mediaPlayer.start();
                }
                else if(!mediaPlayer.isPlaying()){
                    mediaPlayer.seekTo(pauseCurrentPosition);
                    mediaPlayer.start();
                }
                break;

            case R.id.pausebtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.pause();
                    pauseCurrentPosition=mediaPlayer.getCurrentPosition();

                }

                break;

            case R.id.stopbtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.stop();
                    mediaPlayer = null;
                }
                break;

            case R.id.introbtn:
                startActivity(new Intent(Animal5.this,Text5.class));
        }
    }

}
