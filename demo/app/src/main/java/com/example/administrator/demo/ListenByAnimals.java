package com.example.administrator.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

public class ListenByAnimals extends AppCompatActivity {
    ImageButton btn1, btn2, btn3, btn4, btn5, btn6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listen_by_animals);

        btn1 = (ImageButton) findViewById(R.id.imagebtn1);
        btn2 = (ImageButton) findViewById(R.id.imagebtn2);
        btn3 = (ImageButton) findViewById(R.id.imagebtn3);
        btn4 = (ImageButton) findViewById(R.id.imagebtn4);
        btn5 = (ImageButton) findViewById(R.id.imagebtn5);
        btn6 = (ImageButton) findViewById(R.id.imagebtn6);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ListenByAnimals.this,Animal1.class);
                startActivity(intent);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ListenByAnimals.this,Animal2.class);
                startActivity(intent);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ListenByAnimals.this,Animal3.class);
                startActivity(intent);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ListenByAnimals.this,Animal4.class);
                startActivity(intent);
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ListenByAnimals.this,Animal5.class);
                startActivity(intent);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ListenByAnimals.this,Animal6.class);
                startActivity(intent);
            }
        });

    }
}
