package com.example.administrator.demo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class scene1 extends AppCompatActivity implements View.OnClickListener{
    ImageButton play, pause, stop;
    Button introduction;
    MediaPlayer mediaPlayer;
    int pauseCurrentPosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scene1);

        TextView textView = findViewById(R.id.intro1);
        String text = "100 years ago Mulligans Flat’s dawn chorus would have been richer in sound – with more birds and different species. Regent Honeyeaters would have been fighting over gum blossom nectar, Brown Treecreepers foraging under bark, babblers chatting to eachother as they fosicked for insects. When the sun came up you might hear the distant muffled raw of the Australian Bustard putting on an elaborate performance for a potential lover. In the open woodlands female emus would start to drum their voice-boxes as they strode through the crisp golden grass. You might hear a Brolga call, while performing a nimble silhouetted dance in the early morning light. Hooded Robins would be pouncing on insects, and the low croaky growl of a koala might echo through the eucalypt leaves.\n" +
                "\n" +
                "\n" +
                "With the help of our predator-proof fence, conservation research and community support, we hope to bring back many of these species and hear their call once again.\n" +
                "\n" +
                "\n" +
                "Listen out for these animals\n" +
                " \n" +
                "1.\tRegent Honeyeater (locally extinct)\n" +
                "2.\tWhite-browed Babbler (locally extinct)\n" +
                "3.\tGrey-crowned Babbler (locally extinct)\n" +
                "4.\tAustralian Bustard (locally extinct)\n" +
                "5.\tHooded Robin (locally extinct)\n" +
                "6.\tEmu (locally extinct)\n" +
                "7.\tBrolga (locally extinct)\n" +
                "8.\tBrown Treecreeper (locally extinct)\n" +
                "9.\tKoala\n" +
                "10.\tYellow-rumped Thornbill\n" +
                "11.\tScarlet Robin\n" +
                "12.\tGrey Butcherbird\n" +
                "13.\tKookaburra\n" +
                "14.\tWhite-winged Chough\n" +
                "15.\tCrimson Rosella\n" +
                "16.\tWhite-throated Gerygone\n" +
                "17.\tBlack Firetail Cicada\n" +
                "18.\tTree Cricket\n" +
                "19.\tSuperb Parrot\n" +
                "20.\tSpotted Pardalote\n" +
                "21.\tNoisy Friarbird\n" +
                "22.\tMagpie\n" +
                "23.\tWillie Wagtail\n" +
                "24.\tGrey Fantail\n" +
                "25.\tRufous Whistler\n" +
                "26.\tHorsfield’s Bronze-cuckoo\n" +
                "27.\tFan-tailed Cuckoo\n" +
                "28.\tAustralian Raven\n" +
                "29.\tGrey Currawong\n" +
                "30.\tRed Wattlebird\n" +
                "31.\tGrey Shrike-thrush\n" +
                "32.\tBuff-rumped Thornbill\n" +
                "33.\tWhite-throated Treecreeper\n" +
                "34.\tBrown-headed Honeyeater\n" +
                "35.\tCommon Bronzewing Pigeon\n";

        SpannableString ss = new SpannableString(text);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene1.this, Animal3.class);
                startActivity(intent);
            }
        };
        ClickableSpan clickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene1.this, Animal5.class);
                startActivity(intent);
            }
        };
        ClickableSpan clickableSpan3 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene1.this, Animal2.class);
                startActivity(intent);
            }
        };
        ClickableSpan clickableSpan4 = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(scene1.this, Animal4.class);
                startActivity(intent);
            }
        };

        ss.setSpan(clickableSpan1, 971, 991, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan2, 1297, 1315, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan3, 1466, 1483, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan4, 1573, 1592, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());


        play = (ImageButton) findViewById(R.id.button2);
        pause = (ImageButton) findViewById(R.id.pausebtn);
        stop = (ImageButton) findViewById(R.id.stopbtn);

        play.setOnClickListener(this);
        pause.setOnClickListener(this);
        stop.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.button2:
                if(mediaPlayer==null) {
                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.scene1);
                    mediaPlayer.start();
                }
                else if(!mediaPlayer.isPlaying()){
                    mediaPlayer.seekTo(pauseCurrentPosition);
                    mediaPlayer.start();
                }
                break;

            case R.id.pausebtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.pause();
                    pauseCurrentPosition=mediaPlayer.getCurrentPosition();

                }

                break;

            case R.id.stopbtn:
                if(mediaPlayer!=null) {
                    mediaPlayer.stop();
                    mediaPlayer = null;
                }
                break;

        }
    }
}